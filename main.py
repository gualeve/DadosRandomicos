#!/usr/bin/env python3
'''
Dados Randomicos
Gera uma lista de alunos (matrícula e Nome)
Autor: José Adalberto Façanha Gualeve
Data: 28 Out 2021
Versão : 1.0

Obs.: Não foi feito checagem de homônimos, eles podem ocorrer.
'''

from random import randint, choice, seed
from datetime import date as dt

seed(1037)
lista_nomes = [nome.rstrip() for nome in open("Nomes.csv", mode="r", encoding='utf-8')]
lista_sobrenomes = [sobrenome.rstrip() for sobrenome in open('Sobrenomes.csv', mode = 'r', encoding='utf-8')]

matricula = int(str(dt.today().year)[2:] + '12120000')
saida = []
tamanho = int(input("Tamanho do arquivo: "))
for i in range(tamanho):
    matricula = matricula + randint(2,15)
    nome = choice(lista_nomes)
    sobrenome = choice(lista_sobrenomes)
    if randint(0,2) > 0:
        '''Randomiza se usa dois sobrenomes ou apenas 1'''
        lista_sobrenomes.remove(sobrenome)
        segundo_sobrenome = choice(lista_sobrenomes)
        lista_sobrenomes.append(sobrenome)
        sobrenome = sobrenome + ' ' + segundo_sobrenome

    saida.append(str(matricula) + ':' + nome.upper() + ' ' + sobrenome)

with open('Alunos.txt', 'w') as alunos:
    for linha in saida:
        alunos.write(linha + '\n')
